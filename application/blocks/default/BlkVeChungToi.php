<?php
class BlockDefault_BlkVeChungToi extends Zend_View_Helper_Abstract 
{
    public function blkVeChungToi($template = 'default', $options = null) 
    {
        $view  		= $this->view;
        $arrParam 	= $view->arrParam;
        
        $db = Zend_Registry::get('connectDb');
        $select = $db -> Select()
                      -> from('categories as cate')
                      -> where('cate.status =?', 1)
                      -> order('cate.id DESC');
        
        $row = $db->fetchAll($select);
        
        if (count($row) > 0) {
            include(BLOCK_PATH_DEFAULT . '/BlkVeChungToi/'.$template.'.php');
        }
    }
}