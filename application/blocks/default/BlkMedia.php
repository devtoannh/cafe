<?php
class BlockDefault_BlkMedia extends Zend_View_Helper_Abstract 
{
    public function blkMedia($template = 'default', $options = null) 
    {   
        $view  		= $this->view;
        $arrParam 	= $view->arrParam;
        
        $db = Zend_Registry::get('connectDb');
        
        $select = $db -> Select()
                      -> from('images AS img')
                      -> where('img.status = ?', 1)
                      -> order('img.id DESC');
        $row    = $db -> fetchAll($select);
        if (count($row) > 0) {
            include(BLOCK_PATH_DEFAULT . '/BlkMedia/'.$template.'.php');
        }   
    }
}