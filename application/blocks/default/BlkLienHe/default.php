
<!-- CONTACT US -->
<section id="contact" class="contact section">

	<div class="contact-first">

		<!-- OVERLAY -->
		<div class="awe-overlay overlay-default"></div>
		<!-- END / OVERLAY -->

		<div class="section-content wow fadeInUp" data-wow-delay=".3s">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="contact-body text-center">
							<h3 class="lg text-uppercase">Liên Hệ</h3>
							<hr class="_hr">
							<address class="address-wrap">
								<span class="address">Cafe ToanNH - Đông Anh- Hà Nội</span> <span
									class="phone">+84 906 252 152</span>
							</address>
						</div>

						<div class="see-map text-center">
							<a href="#" data-see-contact="Thông tin liên hệ"
								data-see-map="Địa điểm của chúng tôi"
								class="awe-btn awe-btn-5 awe-btn-default text-uppercase"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- MAP -->
		<div id="map" data-map-zoom="16"
			data-map-latlng="21.109253,105.79738"
			data-snazzy-map-theme="grayscale"
			data-map-marker="<?php echo $view->imgUrl?>/marker.png"
			data-map-marker-size="200*60"></div>
		<!-- END / MAP -->
	</div>

	<div class="contact-second tb">
		<!-- CONTACT FORM -->
		<div class="tb-cell">
			<div class="contact-form contact-form-1">
				<div class="inner wow fadeInUp" data-wow-delay=".3s">
					<form id="send-message-form"
						action="<?php echo $view->baseUrl('contact/ajax/lienhe')?>"
						method="post">
						<div id="contact-content" class="alert alert-success" role="alert">
						  Thông tin của bạn đã gửi thành công
						</div>
						<div class="form-item form-textarea">
							<textarea placeholder="Thông điệp của bạn" name="message"></textarea>
						</div>
						<div class="form-item form-type-name">
							<input type="text" placeholder="Tên của bạn" name="name">
						</div>
						<div class="form-item form-type-email">
							<input type="text" placeholder="Email của bạn"  name="email">
						</div>
						<div class="clearfix"></div>
						<div class="form-actions text-center">
							<input type="submit" value="Gửi thông điệp"
								class="contact-submit awe-btn awe-btn-6 awe-btn-default text-uppercase">
						</div>
						
					</form>
				</div>
			</div>
		</div>
		<!-- END / CONTACT FORM -->
	</div>

</section>

<!-- END / CONTACT US -->