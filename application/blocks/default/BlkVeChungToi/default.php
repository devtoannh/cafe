<!-- GOOD FOOD -->
<section id="good-food" class="good-food section pd">
	<div class="container">
		<div class="good-food-heading text-center">
			<div class="good-food-title style-1 wow fadeInUp"
				data-wow-delay=".2s">
				<i class="icon awe_quote_left"></i>
				<h2 class="lg text-uppercase"><?php $siteConfig = Zend_Registry::get('siteConfig'); echo $siteConfig['config_company']['trichdan'];?></h2>
				<i class="icon awe_quote_right"></i>
			</div>
			<p class=" wow fadeInUp" data-wow-delay=".4s"><?php echo $siteConfig['config_company']['noidungtrichdan'];?></p>
		</div>

		<div class="good-food-body wow fadeInUp" data-wow-delay=".6s">
			<div class="row">
			<?php $class = 12/(count($row));?>
			<?php foreach ($row as $key => $value) 
			{
			    $title = $value['title'];
			    $img   = $value['image'];
			    $note  = $value['note'];
			?>
			<!-- GOOD ITEM -->
				<div class="col-md-<?php echo $class?>">
					<div class="good-item text-center">
						<div class="item-image-head">
							<img src="<?php echo $img ?>" alt="<?php echo $title ?>">
						</div>

						<div class="item-title">
							<h4 class="text-uppercase"><?php echo $title ?></h4>
						</div>
						<div class="item-body">
							<p><?php echo $note ?></p>
						</div>

					</div>
				</div>
				<!-- END / GOOD ITEM -->
			<?php 
			} 
			?>
				<div class="col-md-12 text-center">
					<div class="item-footer scroll-to-menu">
						<a href="#"
							class="awe-btn awe-btn-2 awe-btn-default text-uppercase">Thực đơn</a>
					</div>
				</div>


			</div>
		</div>
	</div>
	<div class="divider divider-2"></div>
</section>
<!-- END / GOOD FOOD -->
