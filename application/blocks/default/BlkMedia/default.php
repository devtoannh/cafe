<!-- HOME MEDIA -->
<section id="home-media" class="home-media section">
	<div class="home-fullscreen tb">
		<ul class="home-slider" data-background="awe-parallax">
    		<?php 
                foreach ($row as $key => $value) {
                    $file  = $value['file'];
                    $title = $value['title'];
                    $note  = $value['note'];
            ?>
			<li>
				<div class="image-wrap">
					<img src="<?php echo $file ?>" alt="<?php echo $title ?>">
				</div>
				<div class="slider-content text-center">
					<h5 class="sm text-uppercase"><?php echo $title ?></h5>
					<h1 class="fittext sbig text-uppercase"><?php echo $note ?></h1>
				</div>
			</li>
			<?php 
			}
			?>
		</ul>
	</div>
</section>
<!-- END / HOME MEDIA -->