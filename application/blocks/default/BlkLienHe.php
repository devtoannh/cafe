<?php
class BlockDefault_BlkLienHe extends Zend_View_Helper_Abstract 
{
    public function blkLienHe($template = 'default', $options = null)
    {
        $view  		= $this->view;
        $arrParam 	= $view->arrParam;
        
        include(BLOCK_PATH_DEFAULT . '/BlkLienHe/' . $template . '.php');
    }
}