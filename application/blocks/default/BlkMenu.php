<?php
class BlockDefault_BlkMenu extends Zend_View_Helper_Abstract 
{
    public function blkMenu($imgUrl = null,$jsUrl = null,$template = 'default', $options = null) 
    {
        $view  		= $this->view;
        $arrParam 	= $view->arrParam;
        
        $db = Zend_Registry::get('connectDb');

        $selectCate = $db -> Select()
                          -> from('categories as cate')
                          -> where('cate.status =?', 1)
                          -> order('cate.id DESC');
        $rowCate    = $db ->fetchAll($selectCate);
        foreach ($rowCate as $key => $value)
        {
            //Get data by id categories
            $selectPro = $db -> Select()
                            -> from('products as pro')
                            -> where('pro.status =?', 1)
                            -> where('pro.categories_id =?', $value['id'])
                            -> order('pro.id DESC');
            $rowPro    = $db ->fetchAll($selectPro);
            $rowCate[$key]['items'] = $rowPro;
        }
        
        include(BLOCK_PATH_DEFAULT . '/BlkMenu/'.$template.'.php');
    }
}