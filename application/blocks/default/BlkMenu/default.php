<!-- THE MENU -->
<section id="the-menu" class="the-menu section">
	<div class="section-heading text-center">
		<!-- BACKGROUND -->
		<div class="awe-parallax bg-3"></div>
		<!-- END / BACKGROUND -->

		<!-- OVERLAY -->
		<div class="awe-overlay"></div>
		<!-- END / OVERLAY -->
		<div class="awe-title awe-title-1 wow fadeInUp" data-wow-delay=".3s">
			<h3 class="lg text-uppercase">THỰC ĐƠN</h3>
		</div>
	</div>

	<div class="tabs-menu tabs-page">
		<div class="container">
			<ul class="nav-tabs text-center" role="tablist">
			<?php foreach ($rowCate as $key => $value) 
			{
			?>
			 <li class="<?php if($key==0)echo "active" ?>"><a href="#<?php echo $value['title_plain'] ?>" role="tab" data-toggle="tab"><?php echo $value['title'] ?></a></li>
			<?php
			}
			?>
			</ul>
		</div>
	</div>

	<div class="section-content wow fadeInUp" data-wow-delay=".5s">
		<div class="container">
			<div class="tab-menu-content tab-content">
			<?php 
			
			foreach ($rowCate as $key => $value) 
			{
			    $rowPro    = $value['items'];
			    if (count($rowPro) > 0) {
			?>
				<!-- BREAKFAST -->
				<div class="tab-pane fade <?php if( $key==0 ) echo "in active" ?>" id="<?php echo $value['title_plain']?>">
					<div class="row">
					<?php foreach ($rowPro as $key => $valuePro) 
					{
					    $tieude     = $valuePro['title'];
					    $gia        = $valuePro['price'];
					    $dongia     = $valuePro['unit'];
					    $anh        = $valuePro['image'];
					    $nguyenlieu = $valuePro['materials'];
					?>
					<!-- THE MENU ITEM -->
						<div class="col-lg-6">
							<div class="the-menu-item">
								<div class="image-wrap">
									<img src="<?php echo $anh ?>" alt="<?php echo $tieude ?>">
								</div>
								<div class="the-menu-body">
									<h4 class="xsm"><?php echo $tieude?></h4>
									<p style="height: 50px;">Nguyên liệu “<b><?php echo $nguyenlieu?></b>”</p>
								</div>
								<div class="prices">
									<span class="price xsm"><?php echo number_format($gia,0,',','.');//$dongia . " " . $gia; ?></span>
								</div>
							</div>
						</div>
						<!-- END / THE MENU ITEM -->
					<?php 
					}?>
				</div>
				<!-- END / BREAKFAST -->
			</div>
			<?php
			    }
			}?>
		</div>
	</div>
	</div>
</section>
<!-- END / THE MENU -->