<?php
class Contact_Model_Ajax extends Zend_Db_Table{
	
	protected $_name = 'contact';
	protected $_primary = 'id';
	
	public function saveItem($arrParam = null, $options = null){
		$db = Zend_Registry::get('connectDb');

		if($options['task'] == 'public-add'){
				$row 				= $this->fetchNew();
				
				$row->name 			= stripslashes($arrParam['name']);
				$row->email 		= stripslashes($arrParam['email']);
				$row->message 		= stripslashes($arrParam['message']);
				$row->ip            = $_SERVER ['REMOTE_ADDR'];
				$row->created       = @date("Y-m-d H:i:s");
				$row->status 		= 0;
	
				$row->save();
		}
	}
}