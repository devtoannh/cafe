<?php
class Contact_AjaxController extends Zendvn_Controller_Action {
		
	protected $_arrParam;
	protected $_currentController;
	protected $_actionMain;
	protected $_namespace;
	
	public function init(){
		$this->_arrParam = $this->_request->getParams();
		$this->_currentController = '/' . $this->_arrParam['module'] . '/' . $this->_arrParam['controller'];
		$this->_actionMain = '/' . $this->_arrParam['module'] . '/'	. $this->_arrParam['controller'] . '/index';	
	
		//Truyen ra ngoai view
		$this->view->arrParam = $this->_arrParam;
		$this->view->currentController = $this->_currentController;
		$this->view->actionMain = $this->_actionMain;
	
		$siteConfig = Zend_Registry::get('siteConfig');
		$template_path = TEMPLATE_PATH . "/public/" . $siteConfig['template']['site'];
		$this->loadTemplate($template_path, 'template.ini', 'index');
	}
	public static function randomStr($lengthgth = 6, $type = '') {
	    $base = '';
	    if ($type == 'numeric') {
	        $base = '0123456789';
	    } elseif ($type == 'string') {
	        $base = 'ABCDEFGHKLMNOPQRSTWXYZabcdefghjkmnpqrstwxyz';
	    } else {
	        $base = 'ABCDEFGHKLMNOPQRSTWXYZabcdefghjkmnpqrstwxyz123456789';
	    }
	    $max = strlen ( $base ) - 1;
	    $str = '';
	    mt_srand ( ( double ) microtime () * 1000000 );
	    while ( strlen ( $str ) < $lengthgth )
	        $str .= $base {mt_rand ( 0, $max )};
	    return $str;
	}
	public function lienheAction(){
	    $_SESSION['Captcha'] = self::randomStr(4, 'numeric');
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		header('Content-Type: application/json');
		$tblAjax = new Contact_Model_Ajax();
		
		if($this->_request->isPost()){
				$tblAjax->saveItem($this->_arrParam,array('task'=>'public-add'));
				echo json_encode($this->_arrParam);
		}
	}
}
