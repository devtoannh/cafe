<?php
class AdminContactController extends Zendvn_Controller_Action{
	
	//Mang tham so nhan duoc khi mot Action chay
	protected $_arrParam;
	
	//Duong dan cua Controller
	protected $_currentController;
	
	//Duong dan cua Action chinh
	protected $_actionMain;
	
	//Thong so phan trang
	protected $_paginator = array(
									'itemCountPerPage' => 15,
									'pageRange' => 10,
									'currentPage' => 1
									);
	protected $_namespace;
	
	//Url page
	protected $_page = '';
	
	public function init(){
		//Mang tham so nhan duoc khi mot Action chay
		$this->_arrParam = $this->_request->getParams();
	
		//Duong dan cua Controller
		$this->_currentController = '/' . $this->_arrParam['module'] . '/' . $this->_arrParam['controller'];
	
		//Duong dan cua Action chinh
		$this->_actionMain = '/' . $this->_arrParam['module'] . '/'	. $this->_arrParam['controller'] . '/index';	
	
		//Luu cac du lieu filter vaof SESSION
		//Dat ten SESSION
		$this->_namespace = $this->_arrParam['module'] . '-' . $this->_arrParam['controller'];
		$ssFilter = new Zend_Session_Namespace($this->_namespace);
		//Lay thong tin so phan tu tren mot trang
		if(isset($this->_arrParam['limitPage'])){
			$ssFilter->limitPage = $this->_request->getParam('limitPage');
			$this->_paginator['itemCountPerPage'] = $ssFilter->limitPage;
		}elseif(!empty($ssFilter->limitPage)){
			$this->_paginator['itemCountPerPage'] = $ssFilter->limitPage;
		}
		
		//Trang hien tai
		if(isset($this->_arrParam['page'])){
			$this->_paginator['currentPage'] = $this->_arrParam['page'];
			$this->_page = '/page/' . $this->_arrParam['page'];
		}
		
		//Truyen thong tin phan trang vao mang du lieu
		$this->_arrParam['paginator'] = $this->_paginator;
		
		//$ssFilter->unsetAll();
		if(empty($ssFilter->col)){
			$ssFilter->keywords = '';
			$ssFilter->col 		= 'c.id';
			$ssFilter->id 		= 'DESC';
			$ssFilter->order 	= 'DESC';
		}
		$this->_arrParam['ssFilter']['keywords'] 	= $ssFilter->keywords;
		$this->_arrParam['ssFilter']['col'] 		= $ssFilter->col;
		$this->_arrParam['ssFilter']['order'] 		= $ssFilter->order;

		if(empty($ssFilter->lang_code)){
			$language = new Zend_Session_Namespace('language');
			$this->_arrParam['ssFilter']['lang_code'] = $language->lang;
		}else{
			$this->_arrParam['ssFilter']['lang_code'] 	= $ssFilter->lang_code;
		}

		//Truyen ra ngoai view
		$this->view->arrParam = $this->_arrParam;
		$this->view->currentController = $this->_currentController;
		$this->view->actionMain = $this->_actionMain;
	
		$siteConfig = Zend_Registry::get('siteConfig');
		$template_path = TEMPLATE_PATH . "/admin/" . $siteConfig['template']['admin'];
		$this->loadTemplate($template_path, 'template.ini', 'template');
	}

	public function indexAction(){
		$this->view->Title = 'Danh sách liên hệ';
		$this->view->headTitle($this->view->Title, true);
		
		$tblContact = new Default_Model_Contact();
		$this->view->Items = $tblContact->listItem($this->_arrParam, array('task'=>'admin-list'));
		
		$totalItem = $tblContact->countItem($this->_arrParam, array('task'=>'admin-list'));
		
		$paginator = new Zendvn_Paginator();
		$this->view->paginator = $paginator->createPaginator($totalItem, $this->_paginator);
	}
	
	public function sentmailAction(){
	     header('Content-Type: application/json');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $tblContact = new Default_Model_Contact();
        $tblContact->changeStatus($this->_arrParam);
//         print_r($this->_arrParam);
	    //Gui mail
        $siteConfig = Zend_Registry::get('siteConfig');
	    $options = $siteConfig['config_mail'];
	    
	    $options['smtpsecure'] 	= $siteConfig['config_mail']['smtpsecure'];
	    $options['smtphost'] 	= $siteConfig['config_mail']['smtphost'];
	    $options['smtpport'] 	= $siteConfig['config_mail']['smtpport'];
	    $options['smtpuser'] 	= $siteConfig['config_mail']['smtpuser'];
	    $options['smtppass'] 	= $siteConfig['config_mail']['smtppass'];
	    $options['mailfrom'] 	= $siteConfig['config_mail']['mailfrom'];
	    $options['fromname'] 	= $siteConfig['config_mail']['fromname'];
	    $options['tomail'] 		= $this->_arrParam['mailNguoiGui'];//Mail người nhận
	    $options['title'] 		= $siteConfig['config_mail']['fromname'];//Tên người gửi
	    $options['subject'] 	= 'Phản hồi từ CAFE';//Tiêu đề thư
	    
	    $content = $this->_arrParam['message-text'];
	    	
	    $mail = new Zendvn_Phpmailer();
	    echo json_decode($mail->send($options,$content));
	}
	
	public function deleteAction(){
	    echo "test";
	    $this->_helper->layout->disableLayout();
	    $this->_helper->viewRenderer->setNoRender();
	    
		if($this->_request->isPost()){
				if(!empty($this->_arrParam['id'])){
				$tblContact = new Default_Model_Contact();
				$tblContact->deleteItem($this->_arrParam, array('task'=>'admin-delete'));
			}
		}
	}
	public function editAction()
	{
	
	    header('Content-Type: application/json');
	    $this->_helper->layout->disableLayout();
	    $this->_helper->viewRenderer->setNoRender();
	
	
	    $tblAjax = new Default_Model_Contact();
	    $id      = $this->getRequest()->getParam('id');
	    if ($this->_request->isPost()) {
	        echo json_encode($tblAjax->getDataID($id));
	    }
	}

}



