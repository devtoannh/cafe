<?php
class PublicController extends Zendvn_Controller_Action{
	
	//Mang tham so nhan duoc khi mot Action chay
	protected $_arrParam;
	
	//Duong dan cua Controller
	protected $_currentController;
	
	//Duong dan cua Action chinh
	protected $_actionMain;
	
	public function init(){
		//Mang tham so nhan duoc khi mot Action chay
		$this->_arrParam = $this->_request->getParams();
	
		//Duong dan cua Controller
		$this->_currentController = '/' . $this->_arrParam['module'] . '/' . $this->_arrParam['controller'];
	
		//Duong dan cua Action chinh
		$this->_actionMain = '/' . $this->_arrParam['module'] . '/'	. $this->_arrParam['controller'] . '/index';	
	
		//Truyen ra ngoai view
		$this->view->arrParam = $this->_arrParam;
		$this->view->currentController = $this->_currentController;
		$this->view->actionMain = $this->_actionMain;
	
		$this->view->siteConfig = Zend_Registry::get('siteConfig');
		$template_path = TEMPLATE_PATH . "/admin/" . $this->view->siteConfig['template']['admin'];
		$this->loadTemplate($template_path, 'template.ini', 'index');
	}
	
	//Ham chay sau ham action
	public function postDispatch(){
		$siteConfig = Zend_Registry::get('siteConfig');
		$this->view->headMeta()->setName('description',$siteConfig['config_meta']['description']);
		$this->view->headMeta()->setName('keywords',$siteConfig['config_meta']['keywords']);
		$this->view->headMeta()->setHttpEquiv('Refresh',$siteConfig['config_meta']['refresh']);
		$this->view->headMeta()->setHttpEquiv('content-language',$siteConfig['config_meta']['content_language']);
		$this->view->headMeta()->setName('classification',$siteConfig['config_meta']['classification']);
		$this->view->headMeta()->setName('language',$siteConfig['config_meta']['language']);
		$this->view->headMeta()->setName('robots',$siteConfig['config_meta']['robots']);
		$this->view->headMeta()->setName('author',$siteConfig['config_meta']['author']);
		$this->view->headMeta()->setName('copyright',$siteConfig['config_meta']['copyright']);
		$this->view->headMeta()->setName('revisit-after',$siteConfig['config_meta']['revisit_after']);
	}
	
	public function loginAction(){
		$this->view->Title = 'Đăng nhập';
		$this->view->headTitle($this->view->Title, true);
		$currentUrl = '/admin';
		
		if($this->_arrParam['action'] != 'login')
		{
			$siteConfig = Zend_Registry::get('siteConfig');
			if(!empty($siteConfig['config_site']['site_dir'])) {
				$currentUrl = explode($siteConfig['config_site']['site_dir'] . '/', $_SERVER['REDIRECT_URL']);
				$currentUrl = $currentUrl[1];
			} else {
				$currentUrl = $_SERVER['REDIRECT_URL'];
			}
		}
		
		if($this->_request->isPost()){
			$validator = new Default_Form_ValidateLogin($this->_arrParam);
			if($validator->isError() == true){
				$this->view->messageError = $validator->getMessageError();
				$this->view->Item = $validator->getData();
			} else {
				$auth = new Zendvn_System_Auth();
				if($auth->login($this->_arrParam) == true){
					$info = new Zendvn_System_Info();
					$info->createInfo();

					$this->_redirect($currentUrl);
				} else {
					$error[] = $auth->getError();
					$this->view->messageError = $error;
				}
			}
		}

	}
	
	public function logoutAction(){
		$this->view->Title = 'Logout';
		$this->view->headTitle($this->view->Title, true);
		$auth = new Zendvn_System_Auth();
		$auth->logout();
		
		$info = new Zendvn_System_Info();
		$info->destroyInfo();
		
		$currentUrl = '/admin';
		$this->_redirect($currentUrl);
	}
}