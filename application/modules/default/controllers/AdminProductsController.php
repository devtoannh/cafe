<?php

class AdminProductsController extends Zendvn_Controller_Action
{
    
    // Mang tham so nhan duoc khi mot Action chay
    protected $_arrParam;
    
    // Duong dan cua Controller
    protected $_currentController;
    
    // Duong dan cua Action chinh
    protected $_actionMain;
    
    // Thong so phan trang
    protected $_paginator = array(
        'itemCountPerPage' => 15,
        'pageRange' => 10,
        'currentPage' => 1
    );

    protected $_namespace;
    
    // Url page
    protected $_page = '';

    public function init()
    {
        // Mang tham so nhan duoc khi mot Action chay
        $this->_arrParam = $this->_request->getParams();
        
        // Duong dan cua Controller
        $this->_currentController = '/' . $this->_arrParam['module'] . '/' . $this->_arrParam['controller'];
        
        // Duong dan cua Action chinh
        $this->_actionMain = '/' . $this->_arrParam['module'] . '/' . $this->_arrParam['controller'] . '/index';
        
        // Luu cac du lieu filter vaof SESSION
        // Dat ten SESSION
        $this->_namespace = $this->_arrParam['module'] . '-' . $this->_arrParam['controller'];
        
        // Truyen ra ngoai view
        $this->view->arrParam = $this->_arrParam;
        $this->view->currentController = $this->_currentController;
        $this->view->actionMain = $this->_actionMain;
        
        $siteConfig = Zend_Registry::get('siteConfig');
        $template_path = TEMPLATE_PATH . "/admin/" . $siteConfig['template']['admin'];
        $this->loadTemplate($template_path, 'template.ini', 'template');
    }

    public function indexAction()
    {
        $this->view->Title = 'Danh sách sản phẩm';
        $this->view->headTitle($this->view->Title, true);
        
        $cate = new Default_Model_Categories();
        $this->view->Cate = $cate;
        
        $tblContact = new Default_Model_Products();
        $this->view->Items = $tblContact->listItem($this->_arrParam, array(
            'task' => 'admin-list'
        ));
    }

    public function addAction()
    {
        header('Content-Type: application/json');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $tblAjax = new Default_Model_Products();
        $id = $this->getRequest()->getParam('id');
        if ($this->_request->isPost()) {
            if ($id != 0) {
                $tblAjax->saveItem($this->_arrParam, array(
                    'task' => 'public-update'
                ));
            } else {
                $tblAjax->saveItem($this->_arrParam, array(
                    'task' => 'public-add'
                ));
            }
            echo json_encode($this->_arrParam);
        }
    }

    public function editAction()
    {
        header('Content-Type: application/json');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $tblAjax = new Default_Model_Products();
        $id = $this->getRequest()->getParam('id');
        if ($this->_request->isPost()) {
            echo json_encode($tblAjax->getDataID($id));
        }
    }

    public function deleteAction()
    {
        echo "test";
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        if ($this->_request->isPost()) {
            if (! empty($this->_arrParam['id'])) {
                $tblContact = new Default_Model_Products();
                $upload_path = $_SERVER['DOCUMENT_ROOT'] . ''.$tblContact->getUrlImage($this->_arrParam['id']);
                if(file_exists($upload_path))
                {
                    echo $upload_path;
                    $img=unlink($upload_path);
                    echo "Xoa Duoc";
                }else{
                    echo $upload_path;
                    echo "Khong Xoa Duoc";
                }
                $tblContact->deleteItem($this->_arrParam, array(
                    'task' => 'admin-delete'
                ));
            }
        }
    }
    public function dataAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $get = new Default_Model_Categories();
        header('Content-Type: application/json');
        echo json_encode($get->getData());
    
    }
    public function uploadfileAction()
    {
        $requests = $this->_request->getParams();
        try {
            $mode = $requests['mode'];
            $result = array();
            
            // Create & get path uploader dir
            $upload_path = PUBLIC_PATH . '/uploads/products/';
            if (! is_dir($upload_path)) {
                @mkdir($upload_path, 0755, true);
            }
            
            // Check writeable
            if (! is_writable($upload_path)) {
                die(htmlspecialchars(json_encode(array(
                    'success' => false,
                    'msg' => 'ThÆ° má»¥c lÆ°u áº£nh khÃ´ng thá»ƒ ghi. Vui lÃ²ng thá»­ láº¡i !'
                )), ENT_NOQUOTES));
            }
            
            // list of valid extensions, ex. array("jpeg", "xml", "bmp")
            $allowedExtensions = array(
                'jpeg',
                'jpg',
                'png',
                'bmp',
                'gif'
            );
            
            // max file size in bytes
            $sizeLimit = 2 * 1024 * 1024;
            
            // Create a instance uploader
            $uploader = new Zendvn_qqFileUploader($allowedExtensions, $sizeLimit);
            
            // Upload file
            $result = $uploader->handleUpload($upload_path);
            
            if (isset($result['error'])) {
                die(htmlspecialchars(json_encode(array(
                    'success' => false,
                    'msg' => $result['error']
                )), ENT_NOQUOTES));
            }
            
            // File uploaded name
            $file_uploaded = $upload_path . $result['filename'];
            if ($mode == 'simple') {
                $max_width = 500;
                $max_height = 500;
                
                $filename = trim($requests['qqfile']);
                
                // Rename file after upload
                $filename_new = $filename != '' ? time() . '_' . $filename : time() . '.jpg';
                @rename($upload_path . $result['filename'], $upload_path . $filename_new);
                $result['mode'] = $mode;
                $result['image'] = $filename_new;
                $result['url'] = '/public/uploads/products/' . $filename_new;
                $result['path'] = $upload_path;
            } else {
                
                $max_width = 1024;
                $max_height = 1024;
                
                // Rename file after upload
                $filename_new = time() . '.' . strtolower($result['ext']);
                @fclose($upload_path . $result['filename']);
                if (file_exists($upload_path . $filename_new)) {
                    $filename_new = random_string('unique') . '.' . strtolower($result['ext']);
                }
                @rename($upload_path . $result['filename'], $upload_path . $filename_new);
                
                $result['image'] = $filename_new;
                $result['path'] = '/public/uploads/products/' . $upload_type . '/' . $upload_id . '/' . $filename_new;
                ;
            }
            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
            die();
        } catch (Exception $e) {
            print_r($e);
            die();
        }
    }
}



