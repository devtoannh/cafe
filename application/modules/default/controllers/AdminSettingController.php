<?php

class AdminSettingController extends Zendvn_Controller_Action
{
    
    // Mang tham so nhan duoc khi mot Action chay
    protected $_arrParam;
    
    // Duong dan cua Controller
    protected $_currentController;
    
    // Duong dan cua Action chinh
    protected $_actionMain;
    
    // Thong so phan trang
    protected $_paginator = array(
        'itemCountPerPage' => 15,
        'pageRange' => 10,
        'currentPage' => 1
    );

    protected $_namespace;
    
    // Url page
    protected $_page = '';

    public function init()
    {
        // Mang tham so nhan duoc khi mot Action chay
        $this->_arrParam = $this->_request->getParams();
        
        // Duong dan cua Controller
        $this->_currentController = '/' . $this->_arrParam['module'] . '/' . $this->_arrParam['controller'];
        
        // Duong dan cua Action chinh
        $this->_actionMain = '/' . $this->_arrParam['module'] . '/' . $this->_arrParam['controller'] . '/index';
        
        // Luu cac du lieu filter vaof SESSION
        // Dat ten SESSION
        $this->_namespace = $this->_arrParam['module'] . '-' . $this->_arrParam['controller'];
        $ssFilter = new Zend_Session_Namespace($this->_namespace);
        // Lay thong tin so phan tu tren mot trang
        if (isset($this->_arrParam['limitPage'])) {
            $ssFilter->limitPage = $this->_request->getParam('limitPage');
            $this->_paginator['itemCountPerPage'] = $ssFilter->limitPage;
        } elseif (! empty($ssFilter->limitPage)) {
            $this->_paginator['itemCountPerPage'] = $ssFilter->limitPage;
        }
        
        // Trang hien tai
        if (isset($this->_arrParam['page'])) {
            $this->_paginator['currentPage'] = $this->_arrParam['page'];
            $this->_page = '/page/' . $this->_arrParam['page'];
        }
        
        // Truyen thong tin phan trang vao mang du lieu
        $this->_arrParam['paginator'] = $this->_paginator;
        
        // $ssFilter->unsetAll();
        if (empty($ssFilter->col)) {
            $ssFilter->keywords = '';
            $ssFilter->col = 'c.id';
            $ssFilter->id = 'DESC';
            $ssFilter->order = 'DESC';
        }
        $this->_arrParam['ssFilter']['keywords'] = $ssFilter->keywords;
        $this->_arrParam['ssFilter']['col'] = $ssFilter->col;
        $this->_arrParam['ssFilter']['order'] = $ssFilter->order;
        
        if (empty($ssFilter->lang_code)) {
            $language = new Zend_Session_Namespace('language');
            $this->_arrParam['ssFilter']['lang_code'] = $language->lang;
        } else {
            $this->_arrParam['ssFilter']['lang_code'] = $ssFilter->lang_code;
        }
        
        // Truyen ra ngoai view
        $this->view->arrParam = $this->_arrParam;
        $this->view->currentController = $this->_currentController;
        $this->view->actionMain = $this->_actionMain;
        
        $siteConfig = Zend_Registry::get('siteConfig');
        $template_path = TEMPLATE_PATH . "/admin/" . $siteConfig['template']['admin'];
        $this->loadTemplate($template_path, 'template.ini', 'template');
    }

    public function addAction()
    {
        header('Content-Type: application/json');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $filename = CONFIG_PATH . '/config_system.ini';
//         $filename = '/application/configs/config_system.ini';
        
        $section = 'site-settings';
        $siteSettings = new Zend_Config_Ini($filename, $section);

        $this->view->siteSettings = $siteSettings->toArray();
        
        
        if ($this->_request->isPost()) {
            $config = new Zend_Config_Ini($filename, $section, true);
            if(isset($this->_arrParam['sitename']))
            $config->config_site->sitename = $this->_arrParam['sitename'];
            if(isset($this->_arrParam['offline']))
            $config->config_site->offline = $this->_arrParam['offline'];
            if(isset($this->_arrParam['offline_message']))
            $config->config_site->offline_message = $this->_arrParam['offline_message'];
            if(isset($this->_arrParam['site_domain']))
            $config->config_site->site_domain = $this->_arrParam['site_domain'];
            if(isset($this->_arrParam['site_url']))
            $config->config_site->site_url = $this->_arrParam['site_url'];
            if(isset($this->_arrParam['site_dir']))
            $config->config_site->site_dir = $this->_arrParam['site_dir'];
            if(isset($this->_arrParam['site_logo']))
            $config->config_site->site_logo = $this->_arrParam['site_logo'];
            if(isset($this->_arrParam['site_language_default']))
            $config->config_site->site_language_default = $this->_arrParam['site_language_default'];
            if(isset($this->_arrParam['site_cache']))
            $config->config_site->site_cache = $this->_arrParam['site_cache'];
            if(isset($this->_arrParam['description']))
            $config->config_meta->description = $this->_arrParam['description'];
            if(isset($this->_arrParam['keywords']))
            $config->config_meta->keywords = $this->_arrParam['keywords'];
            if(isset($this->_arrParam['refresh']))
            $config->config_meta->refresh = $this->_arrParam['refresh'];
            if(isset($this->_arrParam['robots']))
            $config->config_meta->robots = $this->_arrParam['robots'];
            if(isset($this->_arrParam['language']))
            $config->config_meta->language = $this->_arrParam['language'];
            if(isset($this->_arrParam['content_language']))
            $config->config_meta->content_language = $this->_arrParam['content_language'];
            if(isset($this->_arrParam['author']))
            $config->config_meta->author = $this->_arrParam['author'];
            if(isset($this->_arrParam['revisit_after']))
            $config->config_meta->revisit_after = $this->_arrParam['revisit_after'];
            if(isset($this->_arrParam['copyright']))
            $config->config_meta->copyright = $this->_arrParam['copyright'];
            if(isset($this->_arrParam['classification']))
            $config->config_meta->classification = $this->_arrParam['classification'];
            if(isset($this->_arrParam['name']))
            
            $config->config_company->name = $this->_arrParam['name'];
            if(isset($this->_arrParam['address']))
            $config->config_company->address = $this->_arrParam['address'];
            if(isset($this->_arrParam['tell']))
            $config->config_company->tell = $this->_arrParam['tell'];
            if(isset($this->_arrParam['fax']))
            $config->config_company->fax = $this->_arrParam['fax'];
            if(isset($this->_arrParam['hotline']))
            $config->config_company->hotline = $this->_arrParam['hotline'];
            if(isset($this->_arrParam['email']))
            $config->config_company->email = $this->_arrParam['email'];
            if(isset($this->_arrParam['slogan']))
            $config->config_company->slogan = $this->_arrParam['slogan'];
            if(isset($this->_arrParam['yahoo']))
            $config->config_company->yahoo = $this->_arrParam['yahoo'];
            if(isset($this->_arrParam['skype']))
            $config->config_company->skype = $this->_arrParam['skype'];
            if(isset($this->_arrParam['facebook']))
            $config->config_company->facebook = $this->_arrParam['facebook'];
            if(isset($this->_arrParam['google']))
            $config->config_company->google = $this->_arrParam['google'];
            if(isset($this->_arrParam['youtube']))
            $config->config_company->youtube = $this->_arrParam['youtube'];
            if(isset($this->_arrParam['twitter']))
            $config->config_company->twitter = $this->_arrParam['twitter'];
            if(isset($this->_arrParam['status']))
            $config->config_mail->status = $this->_arrParam['status'];
            if(isset($this->_arrParam['mailer']))
            $config->config_mail->mailer = $this->_arrParam['mailer'];
            if(isset($this->_arrParam['mailfrom']))
            $config->config_mail->mailfrom = $this->_arrParam['mailfrom'];
            if(isset($this->_arrParam['fromname']))
            $config->config_mail->fromname = $this->_arrParam['fromname'];
            if(isset($this->_arrParam['tomail']))
            $config->config_mail->tomail = $this->_arrParam['tomail'];
            if(isset($this->_arrParam['smtpauth']))
            $config->config_mail->smtpauth = $this->_arrParam['smtpauth'];
            if(isset($this->_arrParam['smtpsecure']))
            $config->config_mail->smtpsecure = $this->_arrParam['smtpsecure'];
            if(isset($this->_arrParam['smtpport']))
            $config->config_mail->smtpport = $this->_arrParam['smtpport'];
            if(isset($this->_arrParam['smtpuser']))
            $config->config_mail->smtpuser = $this->_arrParam['smtpuser'];
            if(isset($this->_arrParam['smtppass']))
            $config->config_mail->smtppass = $this->_arrParam['smtppass'];
            if(isset($this->_arrParam['smtphost']))
            $config->config_mail->smtphost = $this->_arrParam['smtphost'];
            if(isset($this->_arrParam['analytics']))
            $config->config_google->analytics = $this->_arrParam['analytics'];
            if(isset($this->_arrParam['webmaster_tools']))
            $config->config_google->webmaster_tools = $this->_arrParam['webmaster_tools'];
            
            $write = new Zend_Config_Writer_Ini();
           
           echo $write->write($filename, $config);
        }
    }

    public function indexAction()
    {
        $this->view->Title = 'Danh sách sản phẩm';
        $this->view->headTitle($this->view->Title, true);
        
        $filename = CONFIG_PATH . '/config_system.ini';
        
        $section = 'site-settings';
        $siteSettings = new Zend_Config_Ini($filename, $section);
        // print_r($siteSettings->toArray());
        // die();
        $this->view->siteSettings = $siteSettings->toArray();
        
    }
} 

