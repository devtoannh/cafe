<?php

class Default_Model_Images extends Zend_Db_Table
{

    protected $_name = 'images';

    protected $_primary = 'id';

    protected $_ids;

    public function getItem($arrParam = null, $options = null)
    {
        $db = Zend_Registry::get('connectDb');
        // $db = Zend_Db::factory($adapter, $config);
        
        if ($options['task'] == 'admin-info' || $options['task'] == 'admin-edit') {
            $db = Zend_Registry::get('connectDb');
            // $db = Zend_Db::factory($adapter, $config);
            $select = $db->select()
                ->from('contact AS c')
                ->where('c.id = ?', $arrParam['id'], INTEGER);
            
            $result = $db->fetchRow($select);
        }
        
        if ($options['task'] == 'public-contact') {
            $select = $db->select()
                ->from('blocks AS b')
                ->where('b.id = ?', 3, INTERGER);
            $result = $db->fetchRow($select);
        }
        return $result;
    }
    public function getDataID($id = NULL)
    {
        $db = Zend_Registry::get('connectDb');
        $result = array();
        $select = $db->select()
        ->from('images AS c')
        ->where('c.id =?', $id);
    
        $result = $db->fetchAll($select);
    
        return $result;
    }

    public function listItem($arrParam = null, $options = null)
    {
        $db = Zend_Registry::get('connectDb');
        // $db = Zend_Db::factory($adapter, $config);
        if ($options['task'] == 'admin-list') {
            
            $select = $db->select()->from('images AS c')->  order('c.id DESC');
            
            $result = $db->fetchAll($select);
        }
        
        return $result;
    }

    public function saveItem($arrParam = null, $options = null)
    {
        // $AddIP = new Zendvn_Filter_GetAddIp();
        if ($options['task'] == 'public-add') {
            echo "public-add";
            $row = $this->fetchNew();
            
            $row->id = $arrParam['id'];
            
            $row->file = $arrParam['image'];
            $row->title = $arrParam['title'];
            $row->note = $arrParam['note'];
            $row->status = $arrParam['status'];
            $row->created_at = @date("Y-m-d H:i:s");
            $row->updated_at = @date("Y-m-d H:i:s");
            
            $row->save();
        }
        if ($options['task'] == 'public-update') {
            $data = array(
                'file' => $arrParam['image'],
                'title' => $arrParam['title'],
                'note' => $arrParam['note'],
                'status' => $arrParam['status'],
                'updated_at' => @date("Y-m-d H:i:s")
            );
            if (count(array_filter($data)) != count($data)) {
                unset($data['file']);
            }
            $where = 'id=' . $arrParam['id'];
            $this->update($data, $where);
        }
    }

    public function deleteItem($arrParam = null, $options = null)
    {
        if ($options['task'] == 'admin-delete') {
            $where = ' id=' . $arrParam['id'];
            $result = $this->delete($where);
        }
        
        if ($options['task'] == 'admin-delete-muti') {
            $cid = explode(',', $arrParam['cid']);
            if (! empty($cid) && isset($arrParam['cid'])) {
                $ids = implode(',', $cid);
                $where = 'id IN (' . $ids . ')';
                $this->delete($where);
            }
        }
    }
    public function getUrlImage($id=0)
    {
        $db = Zend_Registry::get('connectDb');
        $select = $db->select()
        ->from('images as c', array(
            'file'
        ))
        ->where('id =?', $id);
        $result = $db->fetchOne($select);
    
        return $result;
    }

}