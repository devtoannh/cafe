<?php

class Default_Model_Categories extends Zend_Db_Table
{

    protected $_name = 'categories';

    protected $_primary = 'id';

    protected $_ids;

    public function getItem($arrParam = null, $options = null)
    {
        $db = Zend_Registry::get('connectDb');
        // $db = Zend_Db::factory($adapter, $config);
        
        if ($options['task'] == 'admin-info' || $options['task'] == 'admin-edit') {
            $db = Zend_Registry::get('connectDb');
            // $db = Zend_Db::factory($adapter, $config);
            $select = $db->select()
                ->from('contact AS c')
                ->where('c.id = ?', $arrParam['id'], INTEGER);
            
            $result = $db->fetchRow($select);
        }
        
        if ($options['task'] == 'public-contact') {
            $select = $db->select()
                ->from('blocks AS b')
                ->where('b.id = ?', 3, INTERGER);
            $result = $db->fetchRow($select);
        }
        return $result;
    }

    public function countItem($arrParam = null, $options = null)
    {
        $ssFilter = $arrParam['ssFilter'];
        $db = Zend_Registry::get('connectDb');
        // $db = Zend_Db::factory($adapter, $config);
        if ($options['task'] == 'admin-list') {
            
            $select = $db->select()->from('products AS c', array(
                'COUNT(c.id) AS totalItem'
            ));
            
            $result = $db->fetchOne($select);
        }
        
        if ($options == null) {
            $select = $db->select()->from('contact AS c', array(
                'COUNT(c.id) AS totalItem'
            ));
            
            $result = $db->fetchOne($select);
        }
        return $result;
    }

    public function listItem($arrParam = null, $options = null)
    {
        $db = Zend_Registry::get('connectDb');
        // $db = Zend_Db::factory($adapter, $config);
        if ($options['task'] == 'admin-list') {
            
            $select = $db->select()->from('categories AS c');
            
            $result = $db->fetchAll($select);
        }
        
        return $result;
    }

    public function getDataID($id = NULL)
    {
        $db = Zend_Registry::get('connectDb');
        $result = array();
        $select = $db->select()
            ->from('categories AS c')
            ->where('c.id =?', $id);
        
        $result = $db->fetchAll($select);
        
        return $result;
    }

    public function getTitle($id = NULL)
    {
        $db = Zend_Registry::get('connectDb');
        $select = $db->select()
            ->from('categories as c', array(
            'title'
        ))
            ->where('id =?', $id);
        $result = $db->fetchOne($select);
        
        return $result;
    }

    public function saveItem($arrParam = null, $options = null)
    {
        // $AddIP = new Zendvn_Filter_GetAddIp();
        if ($options['task'] == 'public-add') {
            echo "public-add";
            $row = $this->fetchNew();
            
            $row->id = $arrParam['id'];
            
            $row->title = $arrParam['title'];
            $row->title_plain = $arrParam['title_plain'];
            $row->image = $arrParam['image'];
            $row->note = $arrParam['note'];
            $row->status = $arrParam['status'];
            $row->created_at = @date("Y-m-d H:i:s");
            $row->updated_at = @date("Y-m-d H:i:s");
            
            $row->save();
        }
        if ($options['task'] == 'public-update') {
            echo "public-update";
            $data = array(
                'title' => $arrParam['title'],
                'title_plain' => $arrParam['title_plain'],
                'image' => $arrParam['image'],
                'note' => $arrParam['note'],
                'status' => $arrParam['status'],
                'updated_at' => @date("Y-m-d H:i:s")
            );
            if (count(array_filter($data)) != count($data)) {
                unset($data['image']);
            }
            $where = 'id=' . $arrParam['id'];
            echo $where;
            $this->update($data, $where);
        }
    }

    public function deleteItem($arrParam = null, $options = null)
    {
        if ($options['task'] == 'admin-delete') {
//             unlink($arrParam['image']);
            $where = ' id=' . $arrParam['id'];
            $result = $this->delete($where);
        }
        
        if ($options['task'] == 'admin-delete-muti') {
            $cid = explode(',', $arrParam['cid']);
            if (! empty($cid) && isset($arrParam['cid'])) {
                $ids = implode(',', $cid);
                $where = 'id IN (' . $ids . ')';
                $this->delete($where);
            }
        }
    }

    public function changeStatus($arrParam = null, $options = null)
    {
        $cid = $arrParam['cid'];
        if (count($cid) > 0) {
            if ($arrParam['type'] == 1) {
                $status = 1;
            } else {
                $status = 0;
            }
            
            $id = implode(',', $cid);
            $data = array(
                'status' => $status
            );
            $where = 'id IN (' . $id . ')';
            $this->update($data, $where);
        }
        if ($arrParam['id'] > 0) {
            if ($arrParam['type'] == 1) {
                $status = 1;
            } else {
                $status = 0;
            }
            $data = array(
                'status' => $status
            );
            $where = 'id = ' . $arrParam['id'];
            $this->update($data, $where);
        }
    }
    public function getUrlImage($id=0)
    {
        $db = Zend_Registry::get('connectDb');
        $select = $db->select()
            ->from('categories as c', array(
            'image'
        ))
            ->where('id =?', $id);
        $result = $db->fetchOne($select);
        
        return $result;
    }
    
    public function getData()
    {
        $db = Zend_Registry::get('connectDb');
        $result = array();
        $select = $db->select()
            ->from('categories AS c', array(
            'id',
            'title'
        ))
            ->where('status =?', 1);
        
        $result = $db->fetchAll($select);
        // print_r($db->fetchAll($select));
        
        return $result;
    }
}