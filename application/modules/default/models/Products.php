<?php

class Default_Model_Products extends Zend_Db_Table
{

    protected $_name = 'products';

    protected $_primary = 'id';

    protected $_ids;

    public function getItem($arrParam = null, $options = null)
    {
        $db = Zend_Registry::get('connectDb');
        // $db = Zend_Db::factory($adapter, $config);
        
        if ($options['task'] == 'admin-info' || $options['task'] == 'admin-edit') {
            $db = Zend_Registry::get('connectDb');
            // $db = Zend_Db::factory($adapter, $config);
            $select = $db->select()
                ->from('contact AS c')
                ->where('c.id = ?', $arrParam['id'], INTEGER);
            
            $result = $db->fetchRow($select);
        }
        
        if ($options['task'] == 'public-contact') {
            $select = $db->select()
                ->from('blocks AS b')
                ->where('b.id = ?', 3, INTERGER);
            $result = $db->fetchRow($select);
        }
        return $result;
    }

    public function countItem($arrParam = null, $options = null)
    {
        $ssFilter = $arrParam['ssFilter'];
        $db = Zend_Registry::get('connectDb');
        // $db = Zend_Db::factory($adapter, $config);
        if ($options['task'] == 'admin-list') {
            
            $select = $db->select()->from('products AS c', array(
                'COUNT(c.id) AS totalItem'
            ));
            
            $result = $db->fetchOne($select);
        }
        
        if ($options == null) {
            $select = $db->select()->from('contact AS c', array(
                'COUNT(c.id) AS totalItem'
            ));
            
            $result = $db->fetchOne($select);
        }
        return $result;
    }

    public function listItem($arrParam = null, $options = null)
    {
        $db = Zend_Registry::get('connectDb');
        // $db = Zend_Db::factory($adapter, $config);
        if ($options['task'] == 'admin-list') {
            
            $select = $db->select()->from('products AS c');
            
            $result = $db->fetchAll($select);
        }
        
        return $result;
    }

    public function saveItem($arrParam = null, $options = null)
    {
        // $AddIP = new Zendvn_Filter_GetAddIp();
        if ($options['task'] == 'public-add') {
            
            $row = $this->fetchNew();
            
            $row->categories_id = $arrParam['categories_id'];
            $row->title = $arrParam['title'];
            $row->title_plain = $arrParam['title_plain'];
            $row->price = $arrParam['price'];
            $row->image = $arrParam['image'];
            $row->unit = $arrParam['unit'];
            $row->materials = $arrParam['materials'];
            $row->status = $arrParam['status'];
            $row->created_at = ($arrParam['created_at'] != "") ? $arrParam['created_at'] : @date("Y-m-d H:i:s");
            $row->updated_at = @date("Y-m-d H:i:s");
            
            $row->save();
        }
        if ($options['task'] == 'public-update') {
            $data = array(
                'categories_id' => $arrParam['categories_id'],
                'title' => $arrParam['title'],
                'title_plain' => $arrParam['title_plain'],
                'price' => $arrParam['price'],
                'image' => $arrParam['image'],
                'unit' => $arrParam['unit'],
                'materials' => $arrParam['materials'],
                'status' => $arrParam['status'],
                'updated_at' => @date("Y-m-d H:i:s")
            );
            if (count(array_filter($data)) != count($data)) {
                unset($data['image']);
            }
            $where = 'id=' . $arrParam['id'];
            $this->update($data, $where);
        }
    }

    public function deleteItem($arrParam = null, $options = null)
    {
        if ($options['task'] == 'admin-delete') {
            $where = ' id=' . $arrParam['id'];
            $result = $this->delete($where);
        }
        
        if ($options['task'] == 'admin-delete-muti') {
            $cid = explode(',', $arrParam['cid']);
            if (! empty($cid) && isset($arrParam['cid'])) {
                $ids = implode(',', $cid);
                $where = 'id IN (' . $ids . ')';
                $this->delete($where);
            }
        }
    }

    public function getDataID($id = NULL)
    {
        $db = Zend_Registry::get('connectDb');
        $result = array();
        $select = $db->select()
            ->from('products AS c')
            ->where('c.id =?', $id);
        
        $result = $db->fetchAll($select);
        
        return $result;
    }
    public function getUrlImage($id=0)
    {
        $db = Zend_Registry::get('connectDb');
        $select = $db->select()
        ->from('products as c', array(
            'image'
        ))
        ->where('id =?', $id);
        $result = $db->fetchOne($select);
    
        return $result;
    }
}