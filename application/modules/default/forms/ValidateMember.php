<?php
class Default_Form_ValidateMember{
	
	//Chua nhung thong bao loi cua form
	protected $_messagesError = null;
	
	//MANG CHUA DU LIEU SAU KHI KIEM TRA
	protected $_arrData;
	
	public function __construct($arrParam = array(), $options = null){
		
		//=========================================
		//KIEM TRA user_name
		//=========================================
		if($arrParam['action'] == 'add'){
			$options = array('table'=>'users','field'=>'user_name');
		}else if($arrParam['action'] == 'edit'){
			$clause = ' id !=' . $arrParam['id'];
			$options = array('table'=>'users','field'=>'user_name','exclude'=>$clause);
		}
		
		$validator = new Zend_Validate();
		
		$validator->addValidator(new Zend_Validate_NotEmpty(),true)
					->addValidator(new Zend_Validate_EmailAddress(),true)
					->addValidator(new Zend_Validate_Db_NoRecordExists($options),true);
		
		if(!$validator->isValid($arrParam['user_name'])){
			$message = $validator->getMessages();
			$this->_messagesError['user_name'] = 'Email đăng nhập: ' . current($message);
		}
		
		//=========================================
		//KIEM TRA password
		//=========================================
		$flag = false;
		if($arrParam['action'] == 'add'){
			$flag = true;
		}else if($arrParam['action'] == 'edit'){
			if(!empty($arrParam['password'])){
				$flag = true;
			}
		}
		
		if($flag == true){
			$validator = new Zend_Validate();
			$validator->addValidator(new Zend_Validate_NotEmpty(),true)
						->addValidator(new Zend_Validate_StringLength(3,32),true)
						->addValidator(new Zend_Validate_Regex('#^[a-zA-Z0-9@\#\$%\^&\*\-\+]+$#'),true);
			if(!$validator->isValid($arrParam['password'])){
				$message = $validator->getMessages();
				$this->_messagesError['password'] = 'Mật khẩu: ' . current($message);
			}
		}

		//=========================================
		//KIEM TRA member_hoten
		//=========================================
		$validator = new Zend_Validate();
		$validator	->addValidator(new Zend_Validate_NotEmpty(),true)
					->addValidator(new Zend_Validate_StringLength(2,40),true);
		if(!$validator->isValid($arrParam['member_hoten'])){
			$message = $validator->getMessages();
			$this->_messagesError['member_hoten'] = 'Họ tên: ' . current($message);
		}
		
		//=========================================
		//KIEM TRA status
		//=========================================
		if(empty($arrParam['status']) || !isset($arrParam['status'])){
			$arrParam['status'] = 0;
		}
		
		//=========================================
		//TRUYEN CAC GIA TRI DUNG VAO MANG $_arrData
		//=========================================
		$this->_arrData = $arrParam;
		
	}
	
	//Kiem tra Error
	//return true neu co loi xuat hien
	public function isError(){
		if(count($this->_messagesError) > 0){
			return true;
		}else{
			return false;
		}
	}
	
	//Tra ve mot mang cac thong bao loi
	public function getMessageError(){
		return $this->_messagesError;
	}
	
	//Tra ve mot du lieu sau khi kiem tra
	public function getData($options = null){
		if($options['upload'] == true){
			$this->_arrData['user_avatar'] = $this->uploadFile();
		}
		return $this->_arrData;
	}
	
	//=========================================
	// 1.Upload user_avatar
	// 2.Resize kich thuoc (100x100 va 450x450)
	// 3.Tra ve ten tap tin upload
	//=========================================
	public function uploadFile(){
		//Duong dan den thu muc upload
		$upload_dir = FILE_PATH . '/users/';
		
		//=========================================
		//UPLOAD FILE user_avatar
		//=========================================
		$upload = new Zendvn_File_Upload();
		$fileInfo = $upload->getFileInfo('user_avatar');
		$fileName = $fileInfo['user_avatar']['name'];
		if(!empty($fileName)){
			$fileName = $upload->upload('user_avatar', $upload_dir . '/orignal',array('task'=>'rename'),'user_');
			
			$thumb = Zendvn_File_Images::create($upload_dir . '/orignal/' . $fileName);
			$thumb->resize(100,100)->save($upload_dir . '/img100x100/' . $fileName);
			
			$thumb = Zendvn_File_Images::create($upload_dir . '/orignal/' . $fileName);
			$thumb->resize(450,450)->save($upload_dir . '/img450x450/' . $fileName);
			
			if($this->_arrData['action'] == 'edit'){
				$upload->removeFile($upload_dir . '/orignal/' . $this->_arrData['current_user_avatar']);
				$upload->removeFile($upload_dir . '/img100x100/' . $this->_arrData['current_user_avatar']);
				$upload->removeFile($upload_dir . '/img450x450/' . $this->_arrData['current_user_avatar']);
			}
		}else{
			if($this->_arrData['action'] == 'edit'){
				$fileName = $this->_arrData['current_user_avatar'];
			}
		}
		
		return $fileName;
	}
}


