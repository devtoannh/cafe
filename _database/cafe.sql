-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 17, 2015 at 06:52 AM
-- Server version: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `cafe`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
`id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_plain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `title_plain`, `image`, `note`, `status`, `created_at`, `updated_at`) VALUES
(41, 'Thức Ăn', 'thuc-an', '/public/uploads/categories/1434398842_img-1.jpg', 'Thức Ăn Đảm Bảo Vệ Sinh An Toàn Thực Phẩm', 1, '2015-06-15 20:08:18', '2015-06-15 20:08:55'),
(42, 'Đồ Uống', 'do-uong', '/public/uploads/categories/1434399010_img-2.jpg', 'Đồ Uống Thơm Ngon Sạch Sẽ', 1, '2015-06-15 20:10:11', '2015-06-15 21:21:05');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `message`, `status`, `created`, `updated_at`, `ip`) VALUES
(1, 'Nguyễn Hữu Toàn', 'toanbk@hotmail.com.vn', 'Tao muốn test và kiểm tra mày', 0, '2015-06-15 21:27:28', '0000-00-00 00:00:00', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
`id` int(10) unsigned NOT NULL,
  `file` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `file`, `title`, `note`, `status`, `seen`, `created_at`, `updated_at`) VALUES
(9, '/public/uploads/images/1434402694_img-2.jpg', 'THỨC ĂN ĐẢM BẢO', 'VỆ SINH THỰC PHẨM', 1, 0, '2015-06-14 09:11:51', '2015-06-15 21:11:40'),
(12, '/public/uploads/images/1434402687_img-3.jpg', 'MÓN NGON MỖI NGÀY', 'PHỤC VỤ TẬN TÌNH', 1, 0, '2015-06-14 09:29:09', '2015-06-15 21:11:28');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
`id` int(10) unsigned NOT NULL,
  `categories_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_plain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `image` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `materials` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `categories_id`, `title`, `title_plain`, `price`, `image`, `unit`, `materials`, `status`, `created_at`, `updated_at`) VALUES
(4, 41, 'Bánh Mỳ Gà', 'banh-my-ga', 15000.00, '/public/uploads/products/1434403461_img-1.jpg', '1', 'Bánh Mỳ - Gà Xé', 1, '2015-06-15 21:24:52', '2015-06-15 21:24:52'),
(5, 41, 'Bánh Xu Kem', 'banh-xu-kem', 10000.00, '/public/uploads/products/1434403502_img-2.jpg', '1', 'Bơ Sữa Kem', 1, '2015-06-15 21:25:22', '2015-06-15 21:25:22'),
(6, 41, 'Bánh Cháy', 'banh-chay', 35000.00, '/public/uploads/products/1434403529_img-3.jpg', '1', 'Kẹo Đắng -  Bột Mỳ', 1, '2015-06-15 21:25:57', '2015-06-15 21:25:57'),
(7, 42, 'Cà Phê Cappuccino', 'ca-phe-cappuccino', 50000.00, '/public/uploads/products/1434403566_img-4.jpg', '1', 'Cà Phê Ý', 1, '2015-06-15 21:26:28', '2015-06-15 21:26:28'),
(8, 41, 'Sinh Tố Nho', 'sinh-to-nho', 45000.00, '/public/uploads/products/1434403593_img-5.jpg', '1', 'NHo - Đường', 1, '2015-06-15 21:26:48', '2015-06-15 21:26:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
`id` int(10) unsigned NOT NULL,
  `user_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `email`, `password`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'abc@gmail.com', '7e1fff26e4fcd3bc7063dcbdbf7e4768', 1, '2015-04-27 09:38:18', '2015-04-27 09:38:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `product_types_title_unique` (`title`), ADD UNIQUE KEY `product_types_title_plain_unique` (`title_plain`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`id`,`categories_id`), ADD UNIQUE KEY `products_title_unique` (`title`), ADD UNIQUE KEY `products_title_plain_unique` (`title_plain`), ADD KEY `categories_id` (`categories_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_name_unique` (`user_name`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
ADD CONSTRAINT `products_categories_id_foreign` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`);
