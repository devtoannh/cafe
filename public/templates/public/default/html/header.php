<?php echo $this->doctype() ?>
<?php
	$siteConfig = Zend_Registry::get('siteConfig');
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->headTitle() ?>
    <?php echo $this->headMeta() ?>
	<?php echo $this->headLink() ?>
	<?php echo $this->headScript() ?>
	<script type="text/javascript">var base_url = "<?php echo $siteConfig['config_site']['site_url'];?>";</script>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	
	<link rel="stylesheet" type="text/css" href="<?php echo $this->cssUrl; ?>/gallery.css" />
	<script type="text/javascript" src="<?php echo $this->jsUrl; ?>/highslide-with-gallery.js"></script>
	<script type="text/javascript">
		hs.graphicsDir = '<?php echo $this->imgUrl; ?>/graphics/';
		hs.align = 'center';
		hs.transitions = ['expand', 'crossfade'];
		hs.outlineType = 'rounded-white';
		hs.fadeInOut = true;
		hs.numberPosition = 'caption';
		hs.dimmingOpacity = 0.75;
	
		// Add the controlbar
		if (hs.addSlideshow) hs.addSlideshow({
			//slideshowGroup: 'group1',
			interval: 5000,
			repeat: false,
			useControls: true,
			fixedControls: true,
			overlayOptions: {
				opacity: .75,
				position: 'bottom center',
				hideOnMouseOut: true
			}
		});
	</script>
</head>
<body class="body">