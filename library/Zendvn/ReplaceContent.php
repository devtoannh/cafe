<?php
class Zendvn_ReplaceContent{
		
	public function replace($content){
		$siteConfig = Zend_Registry::get('siteConfig');
		$pattern = '#(<a href="(.*?)">)?\[(.*?)(\|\|(.*?))?\]#iu';
		preg_match($pattern, $content, $m);
		if(empty($m)) {
			return $content;
		}
		$random = rand(0,1000000);
		if(strtolower(trim($m[3])) == 'audio') {
			$audio_short 	= '<span class="audio"><a href="'.$m[2].'" class="sm2_button" onclick="return false" rel="nofollow">Nghe</a></span>';
			$audio_long 	= '<ul class="playlist"><li><a href="'.$m[2].'" rel="nofollow">Click để nghe audio</a></li></ul>';
			if(!empty($m[2])) {
				if(!isset($m[5])) {
					$r_str = $audio_short;
				} else {
					if(strtolower(trim($m[5])) == 'short') {
						$r_str = $audio_short;
					}
					if(strtolower(trim($m[5])) == 'long') {
						$r_str = $audio_long;
					}
				}
			}
		}
		
		if(strtolower(trim($m[3])) == 'video') {
			$width = 500;
			$height = 400;
			if(!empty($m[2])) {
				if(isset($m[5])) {
					$arrOption = @explode(';', $m[5]);
					$arrWidth = @explode(':', $arrOption[0]);
					$arrHeight = @explode(':', $arrOption[1]);
					$width = $arrWidth[1];
					$height = $arrHeight[1];
				}
				$r_str	= '<div id="video_'.$random.'">Loading Video...</div><script type="text/javascript">jwplayer("video_'.$random.'").setup({file: "'.$m[2].'",width: '.$width.',height: '.$height.'});</script>';
				//$r_str 	= '<script type="text/javascript">$(document).ready(function(){jplayVideoMedia(\'video'.$random.'\', \''.$siteConfig['config_site']['site_domain'].$m[2].'\', \'\', \''.$width.'\', \''.$height.'\');});</script><div id="jp_container_video'.$random.'" class="jp-video jp-video-360p"><div class="jp-type-single"><div id="play_video_video'.$random.'" class="jp-jplayer"></div><div class="jp-gui"><div class="jp-video-play"><a class="jp-video-play-icon" tabindex="1"><span>play</span></a></div><div class="jp-interface"><div class="jp-controls-holder"><ul class="jp-controls"><li><a class="jp-play" tabindex="1">play</a></li><li><a class="jp-pause" tabindex="1">pause</a></li><li><span class="jp-current-time"></span></li><li class="jp-bar"><div class="jp-progress"><div class="jp-seek-bar"><div class="jp-play-bar"></div></div></div></li><li><span class="jp-duration"></span></li><li><a class="jp-mute" tabindex="1" title="mute">mute</a></li><li><a class="jp-unmute" tabindex="1" title="unmute">unmute</a></li><li><a class="jp-full-screen" tabindex="1" title="full screen">full screen</a></li><li><a class="jp-restore-screen" tabindex="1" title="restore screen">restore screen</a></li></ul><div class="jp-volume-bar"><div class="jp-volume-bar-value"></div></div></div></div></div></div></div>';
			}
		}
		
		if(strtolower(trim($m[3])) == 'lang') {
			if(isset($m[5])) {
				$arrOption 	= @explode(';', $m[5]);
				$en 		= @explode(':', $arrOption[0]);
				$vi 		= @explode(':', $arrOption[1]);
			}
			$r_str 	= '<span class="lang_en" id="'.$random.'">'.$en[1].'</span><div class="lang_vi" id="vi_'.$random.'">'.$vi[1].'</div>';
		}
		
		$content = preg_replace($pattern, $r_str, $content, 1);
		return $this->replace($content);
	}
}