<?php
class Zendvn_EncodeString{
	public function encode($content){
		$string = base64_encode(trim($content) . 'namnv');
		return $string;
	}
	public function decode($content){
		$string = trim(str_replace('namnv','',base64_decode($content)));
		return $string;
	}
}