<?php
class Zendvn_Plugin_Permission extends Zend_Controller_Plugin_Abstract{
	
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
	
		$auth = Zend_Auth::getInstance();
		
		$moduleName = $this->_request->getModuleName();
		$controllerName = $this->_request->getControllerName();
		$flagAdmin = false;
		if($controllerName == 'admin' || $controllerName == 'user'){
			$flagAdmin = true;
			$permissionController = $controllerName;
		}else{
			$tmp = explode('-', $controllerName);
			if($tmp[0] == 'admin' || $tmp[0] == 'user'){
				$flagAdmin = true;
				$permissionController = $tmp[0];
			}
		}
			
		//echo $permission;
		if($permissionController == 'admin'){
		
			$flagPage = 'none';
			if($flagAdmin == true){
				if($auth->hasIdentity() == false){
					$flagPage = 'login';
				}
			}
		
			if($flagPage != 'none'){
				if($flagPage == 'login'){
					$this->_request->setModuleName('default');
					$this->_request->setControllerName('public');
					$this->_request->setActionName('login');
				}
			}
		}
			
		//----------------- KIEM TRA QUYEN TRUY CAP VAO ADMIN ----------------//
		if($permissionController == 'user'){
			
			$flagPage = 'none';
			if($flagAdmin == true){
				if($auth->hasIdentity() == false){
					$flagPage = 'login';
				}
			}
			
			if($flagPage != 'none'){
				if($flagPage == 'login'){
					$this->_request->setModuleName('member');
					$this->_request->setControllerName('thanh-vien');
					$this->_request->setActionName('login');
				}
			}
		}
	}
}