<?php
class Zendvn_View_Helper_CmsEditor extends Zend_View_Helper_Abstract{
	
	public function cmsEditor($name,$value,$options = null,$width = 'auto',$height = 200,$autoHeight = true,$maxHeight = 500, $upload = true){
		// Include the CKEditor class.
		require_once (SCRIPTS_PATH . "/ckeditor/ckeditor_php5.php");
		
		// Khoi tao CKeditor.
		$CKEditor = new CKEditor();
		
		// Duong dan den thu muc CKeditor tinh tu file chay.
		$CKEditor->basePath = SCRIPTS_URL . '/ckeditor/';
		
		// Thiet lap chieu rong va chieu cao.
		$CKEditor->config['width'] = $width;
		$CKEditor->config['height'] = $height;
		
		if(!isset($options['skin'])){
			$CKEditor->config['skin'] = 'kama';
		}else{
			$CKEditor->config['skin'] = $options['skin'];
		}
		
		// Cau hinh ngon ngu
		if(!isset($options['language'])){
			$CKEditor->config['language'] = 'vi';
		}else{
			$CKEditor->config['language'] = $options['language'];
		}
		//Cau hinh mau nen
		$CKEditor->config['uiColor'] = '#C8E0E4';
		//Khi nhan Enter
		$CKEditor->config['enterMode'] = 'CKEDITOR.ENTER_DIV';
		$CKEditor->config['shiftEnterMode'] = 'CKEDITOR.ENTER_BR';
		
		//Cau hinh tu dong dan chieu cao CKediter den khi dat muc maxHeight se co thanh cuon
		$plugins = 'tableresize,gmap,jwplayer';
		if($autoHeight == true){
			$CKEditor->config['extraPlugins'] = $plugins . ',autogrow,syntaxhighlight';
			$CKEditor->config['autoGrow_maxHeight'] = $maxHeight;
		}
		$CKEditor->config['removePlugins'] = 'resize';
		
		//Xoa dinh dang css khi cop noi dung tu word
		$CKEditor->config['pasteFromWordRemoveFontStyles'] = true;
		//Dinh dang font chu Unicode
		$CKEditor->config['entities'] = false;
		//$CKEditor->config['loaded'] = false;//CKEDitor_loaded = false;
		$CKEditor->config['tabSpaces'] = 10;
		
		//Goi Toolbar nao muon su dung
		if(!isset($options['toolbar'])){
			$CKEditor->config['toolbar'] = 'Full';
		}else{
			$CKEditor->config['toolbar'] = $options['toolbar'];
		}
		
		if($upload == true){
			//Cau hinh upload bang KCFinder
			$CKEditor->config['filebrowserBrowseUrl'] 		= SCRIPTS_URL . '/media_files/browse.php?type=files';
			$CKEditor->config['filebrowserImageBrowseUrl'] 	= SCRIPTS_URL . '/media_files/browse.php?type=images';
			$CKEditor->config['filebrowserFlashBrowseUrl']	= SCRIPTS_URL . '/media_files/browse.php?type=flash';
			$CKEditor->config['filebrowserUploadUrl'] 		= SCRIPTS_URL . '/media_files/upload.php?type=files';
			$CKEditor->config['filebrowserImageUploadUrl'] 	= SCRIPTS_URL . '/media_files/upload.php?type=images';
			$CKEditor->config['filebrowserFlashUploadUrl'] 	= SCRIPTS_URL . '/media_files/upload.php?type=flash';
		}
		
		// Gia tri Value cua CKeditor
		$initialValue = $value;
		
		// Khoi tao 1 textarea va chen gia tri vao cho no.
		return $CKEditor->editor($name, $initialValue);
	}
}