<?php
class Zendvn_View_Helper_CmsReplaceContent extends Zend_View_Helper_Abstract{
	
	public function cmsReplaceContent($s, $options = null){
		if($options == null) {
			$pattern = '#(<a href="(.+?)">)?\[(.+?)(\|\|(.+?))?\]#is';
			preg_match($pattern, $s, $m);
			if(empty($m)) 
				return $s;
			if(strtolower($m[3]) == 'audio') {
				if(!empty($m[2])) {
					if(!isset($m[5])) {
						$r_str = 'Ngắn';
					} else {
						if($m[5] == 'short') {
							$r_str = 'Ngắn';
						}
						if($m[5] == 'long') {
							$r_str = 'Dài';
						}
					}
				}
			}
			$s = preg_replace($pattern, $r_str, $s, 1);
		}
		return $this->cmsReplaceContent($s, $options = null);
	}
}