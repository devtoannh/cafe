<?php
class Zendvn_View_Helper_CmsUserFiles extends Zend_View_Helper_Abstract{
	
	public function cmsUserFiles($name, $value = null, $nameBtn, $type = 'image', $options = array(), $formName = 'appForm'){
		
		$strOptions = '';
		if(count($options)>0){
			foreach ($options as $keyOptions => $valueOptions){
				$strOptions .= $keyOptions . '="' . $valueOptions . '" ';
			}
		}
		
		$typeMedia 	= 'images';
		$xem_media	= '';
		switch ($type){
			case "image": 	$typeMedia = 'images';
							$xem_media = '$(\'.media_'.$name.'\').after(\'<a href="javascript:void(0)" onclick="popup_media(\\\''.$name.'\\\', \\\''.$type.'\\\')" class="note_media media_xem" id="view_'.$name.'">[Xem]</a><a href="javascript:void(0)" onclick="delete_media(\\\''.$name.'\\\')" class="note_media media_xoa" id="delete_'.$name.'">[Xóa]</a>\');';
							break;
			case "images": 	$typeMedia = 'images';
							$xem_media = '$(\'.media_'.$name.'\').after(\'<a href="javascript:void(0)" onclick="popup_media(\\\''.$name.'\\\', \\\''.$type.'\\\')" class="note_media media_xem" id="view_'.$name.'">[Xem]</a><a href="javascript:void(0)" onclick="delete_media(\\\''.$name.'\\\')" class="note_media media_xoa" id="delete_'.$name.'">[Xóa]</a>\');';
							break;
			case "flash": 	$typeMedia = 'flash';
							$xem_media = '$(\'.media_'.$name.'\').after(\'<a href="javascript:void(0)" onclick="popup_media(\\\''.$name.'\\\', \\\''.$type.'\\\')" class="note_media media_xem" id="view_'.$name.'">[Xem]</a><a href="javascript:void(0)" onclick="delete_media(\\\''.$name.'\\\')" class="note_media media_xoa" id="delete_'.$name.'">[Xóa]</a>\');';
							break;
			case "audio": 	$typeMedia = 'files';
							$xem_media = '$(\'.media_'.$name.'\').after(\'<a href="javascript:void(0)" onclick="popup_media(\\\''.$name.'\\\', \\\''.$type.'\\\')" class="note_media media_xem" id="view_'.$name.'">[Xem]</a><a href="javascript:void(0)" onclick="delete_media(\\\''.$name.'\\\')" class="note_media media_xoa" id="delete_'.$name.'">[Xóa]</a>\');';
							break;
			case "video": 	$typeMedia = 'files';
							$xem_media = '$(\'.media_'.$name.'\').after(\'<a href="javascript:void(0)" onclick="popup_media(\\\''.$name.'\\\', \\\''.$type.'\\\')" class="note_media media_xem" id="view_'.$name.'">[Xem]</a><a href="javascript:void(0)" onclick="delete_media(\\\''.$name.'\\\')" class="note_media media_xoa" id="delete_'.$name.'">[Xóa]</a>\');';
							break;
			default:		$typeMedia = 'files';
							$xem_media = '$(\'.media_'.$name.'\').after(\'<a href="javascript:void(0)" onclick="delete_media(\\\''.$name.'\\\')" class="note_media media_xoa" id="delete_'.$name.'">[Xóa]</a>\');';
		}
		
		$linkMedia = SCRIPTS_URL . '/media_files/browse.php?type=' . $typeMedia . '&lng=vi';
		
		$xhtml = '<script type="text/javascript">
			$(document).ready(function(){
				if($(\'#'.$name.'\').val() != \'\'){
					'.$xem_media.'
				}
			});
			function openKCFinder'.$name.'(field) {
				loading();
				window.KCFinder = {
					callBack: function(url) {
						$(\'#'.$name.'\').val(url);
						window.KCFinder = null;
						$(\'#view_'.$name.'\').remove();
						$(\'#delete_'.$name.'\').remove();
						'.$xem_media.'
					}
				};
				window.open(\'' . $linkMedia . '\', \'kcfinder_textbox\',\'status=0, toolbar=0, location=0, menubar=0, directories=0,resizable=1, scrollbars=0, width=950, height=450\' );
				close_loading();
			}</script>';
		
		$xhtml .= '<input value="'.$value.'" type="text" name="'.$name.'" id="'.$name.'"' . $strOptions . ' data="'.$type.'">';
		$xhtml .= '<a href="javascript:void(0);" onclick="openKCFinder'.$name.'('.$formName.'.'.$name.')" class="btnMedia media_'.$name.'">'. $nameBtn .'</a>';
		
		return $xhtml;
	}
}