<?php
class Zendvn_View_Helper_CmsInform extends Zend_View_Helper_Abstract{
	
	public function cmsInform($arrParam = null, $inform = null, $attribs = null){
		if(count($arrParam) > 0){
			$strAttribs = '';
			if(count($attribs)>0){
				foreach ($attribs as $keyAttribs => $valueAttribs){
					$strAttribs .= $keyAttribs . '="' . $valueAttribs . '" ';
				}
			}
			$arrInform = $inform;
			if($arrParam['save'] == 'ok') {
				$arrInform[] = 'Cập nhật dữ liệu thành công';
			}
			
			$xhtml = '';
			if(count($arrInform) > 0) {
				$xhtml = '<ul ' . $strAttribs . '>';
				foreach ($arrInform as $key => $val){
					$xhtml .= '<li> ' . $val . '</li>';
				}
				$xhtml .= '</ul>';
			}
			
			return $xhtml;
		}
	}
}