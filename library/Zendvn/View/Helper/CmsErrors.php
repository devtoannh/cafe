<?php
class Zendvn_View_Helper_CmsErrors extends Zend_View_Helper_Abstract{
	
	public function cmsErrors($messageError = null, $attribs = null){
		if(count($messageError) > 0){
			$strAttribs = '';
			if(count($attribs)>0){
				foreach ($attribs as $keyAttribs => $valueAttribs){
					$strAttribs .= $keyAttribs . '="' . $valueAttribs . '" ';
				}
			}
			
			$xhtml = '<ul ' . $strAttribs . '>';
			foreach ($messageError as $key => $val){
				$xhtml .= '<li> ' . $val . '</li>';
			}
			$xhtml .= '</ul>';
			
			return $xhtml;
		}
	}
}