<?php

class Zendvn_qqFileUploader {
	private $allowedExtensions = array();
	private $sizeLimit = 10485760;
	private $file;

	function __construct(array $allowedExtensions = array(), $sizeLimit = 10485760){
		$allowedExtensions = array_map("strtolower", $allowedExtensions);

		$this->allowedExtensions = $allowedExtensions;
		$this->sizeLimit = $sizeLimit;
		$this->checkServerSettings();

		if (isset($_GET['qqfile'])) {
			$this->file = new Zendvn_qqUploadedFileXhr();
		} elseif (isset($_FILES['qqfile'])) {
			$this->file = new Zendvn_qqUploadedFileForm();
		} else {
			$this->file = false;
		}
	}

	private function checkServerSettings(){
		$postSize = $this->toBytes(ini_get('post_max_size'));
		$uploadSize = $this->toBytes(ini_get('upload_max_filesize'));

		if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
			$size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';
			die("{'error':'increase post_max_size and upload_max_filesize to $size'}");
		}
	}

	private function toBytes($str){
		$val = trim($str);
		$last = strtolower($str[strlen($str)-1]);
		switch($last) {
			case 'g': $val *= 1024;
			case 'm': $val *= 1024;
			case 'k': $val *= 1024;
		}
		return $val;
	}

	/**
	 * Returns array('success'=>true) or array('error'=>'error message')
	 */
	function handleUpload($uploadDirectory, $replaceOldFile = FALSE){
		if (!is_writable($uploadDirectory)){
			return array('error' => "Lỗi máy chủ. Thử mục tải ảnh lên không cho phép ghi.");
		}

		if (!$this->file){
			return array('error' => 'Không có file nào được tải lên.');
		}

		$size = $this->file->getSize();

		if ($size == 0) {
			return array('error' => 'File tải lên trống');
		}

		if ($size > $this->sizeLimit) {
			return array('error' => 'Dung lượng file tải lên quá lớn');
		}
		$pathinfo = pathinfo($this->file->getName());
		$filename = $pathinfo['filename'];
		//$filename = md5(uniqid());
		$ext = $pathinfo['extension'];

		if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
			$these = implode(', ', $this->allowedExtensions);
			return array('error' => 'File tải lên sai định dạng. Định dạng tải lên cho phép '. $these . '.');
		}

		if(!$replaceOldFile){
			/// don't overwrite previous files that were uploaded
			while (file_exists($uploadDirectory . $filename . '.' . $ext)) {
				$filename .= rand(10, 99);
			}
		}

		if ($this->file->save($uploadDirectory . $filename . '.' . $ext)){
			return array('success'=>true, 'filename'=>$filename . '.' . $ext, 'name'=>$filename, 'ext'=>$ext);
		} else {
			return array('error'=> 'Không thể lưu file tải lên.' .
				'Quá trình tải lên bị hủy bỏ hoặc máy chủ bị gián đoạn');
		}

	}
}
 ?>